#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27, 16, 2);

void setup() {
  lcd.init(); // initialize the lcd
  lcd.backlight(); // включить подсветку  

}

void loop() {
  lcd.home();
  lcd.println("Hello, World!");
}
