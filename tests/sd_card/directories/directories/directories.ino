#include <SD.h>
#include <SPI.h>
const int chipSelect = 10;
File fileDS;

void setup () 
{
  // Open serial communications and wait for port to open:
  Serial.begin(9600);

  Serial.print("Initializing SD card...");

  if (!SD.begin(chipSelect)) {
    Serial.println("initialization failed. Things to check:");
    Serial.println("1. is a card inserted?");
    Serial.println("2. is your wiring correct?");
    Serial.println("3. did you change the chipSelect pin to match your shield or module?");
    Serial.println("Note: press reset button on the board and reopen this Serial Monitor after fixing your issue!");
  }

  Serial.println("initialization done.");
  SD.mkdir("/Data/DS");

  fileDS = SD.open("/Data/DS/loger.txt", FILE_WRITE);
  if (fileDS) {
    fileDS.println("Temp: 13.6");
    fileDS.close();
  }
}

void loop() 
{
  
}
