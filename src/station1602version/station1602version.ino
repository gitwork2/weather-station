#include <SPI.h>
#include <Wire.h>
#include <GyverBME280.h>
#include <RTClib.h>
#include <SD.h>
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27, 16, 2);
GyverBME280 bme;
RTC_DS3231  rtc;
File fileBME; // SD file for BME
File fileRTC; // SD file for RTC

const int SDWrite_timer = 10; // timer for sd writig in seconds
uint32_t millTmr_1, millTmr_2, millTmr_3, millTmr_4;
const byte  buttPin = D0;
static byte menuNum = 0;
#include "menu.h"
 
void setup() {
  Serial.begin(9600);
  pinMode(buttPin, INPUT);

  // BME
  bme.begin();
  if (!bme.begin()) {
    Serial.println("Coudn't find BME!");
  } else {
    Serial.println("BME started good!");
  }
  // RTC
  if (!rtc.begin()) {
    Serial.println("Couldn't find RTC");
  } else {
    Serial.println("RTC started good!");
  }
  // Set time and date
  if (rtc.lostPower()) {
    Serial.println("RTC lost power, let's set the time!");
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  }

  // 1602 I2C
  lcd.init(); // initialize the lcd
  lcd.backlight(); // включить подсветку  

  // SD card
  const int chipSelect = 10;
  if (!SD.begin(chipSelect)) {
    Serial.println("initialization failed. Things to check:");
    Serial.println("1. is a card inserted?");
    Serial.println("2. is your wiring correct?");
    Serial.println("3. did you change the chipSelect pin to match your shield or module?");
    Serial.println("Note: press reset button on the board and reopen this Serial Monitor after fixing your issue!");
  } else {
    Serial.println("SD card started good!");
  }
}

void loop() {
/* =============================== MENU =============================== */
  DateTime now = rtc.now();

  // Set sources for bme structure
  bmeInfo weather {
    bme.readTemperature(),
    bme.readHumidity(),
    round(pressureToMmHg(bme.readPressure()) * 10) / 10
  };
  // Set source for rtc structure
  rtcData clockData {
    now.year(),
    now.month(),
    now.day(),
    now.hour(),
    now.minute(),
    now.second(),
    daysOfTheWeek[now.dayOfTheWeek()]
  };

  // Get switch press
  if (millis() - millTmr_1 > 100) {
    Serial.print("Button: ");
    Serial.println(digitalRead(buttPin));
    millTmr_1 = millis();
    if (digitalRead(buttPin)) { 
      ++menuNum;
    }
    if (menuNum > 2) menuNum = 0;    
  }

  if (millis() - millTmr_4 > 700) { // Timer for displaying
    millTmr_4 = millis();
      switch (menuNum) {
      case 0:
        lcd.clear();
        mainMenu(clockData, weather);
        break;
      case 1:
        lcd.clear();
        bmeMenu(weather, bmePressureFlag);
        break;
      case 2:
        lcd.clear();
        rtcMenu(clockData);
        break;
      default:
        break;
    }
  }

  // Write data on SD card
  // Write BME
  if (millis() - millTmr_2 > 1000*SDWrite_timer) { // Timer 
    millTmr_2 = millis();
    SD.mkdir("/MBE");
    // BME Temperature
    fileBME = SD.open ("/BME/Temperature.txt", FILE_WRITE);
    if (fileBME)
      fileBME.println (weather.temp);
      fileBME.close();
    // BME Humidity
    fileBME = SD.open ("/BME/Humidity.txt", FILE_WRITE);
    if (fileBME) 
      fileBME.println (weather.humid);
      fileBME.close();
    // BME Pressure
    fileBME = SD.open ("/BME/Pressure.txt", FILE_WRITE);
    if (fileBME)
      fileBME.println (weather.pres);
      fileBME.close();
  
    // Write RTC
    // RTC Data
    SD.mkdir("/RTC");
    fileRTC = SD.open ("/RTC/Data.txt", FILE_WRITE);
    if (fileRTC)
      fileRTC.print (clockData.rtcYear);
      fileRTC.print ("/");
      fileRTC.print (clockData.rtcMonth);
      fileRTC.print ("/");
      fileRTC.println (clockData.rtcDay);
      fileRTC.close();
    // RTC Time
    fileRTC = SD.open ("/RTC/Time.txt", FILE_WRITE);
    if (fileRTC)
      fileRTC.print (clockData.rtcHour);
      fileRTC.print (":");
      fileRTC.print (clockData.rtcMinute);
      fileRTC.print (":");
      fileRTC.println (clockData.rtcSecond);
      fileRTC.close();

      bool flag = false;
      if (clockData.rtcHour == 0 && flag == false) {
        flag = true;
       
        // BME Temperature
        fileBME = SD.open ("/BME/Temperature.txt", FILE_WRITE);
        if (fileBME)
          fileBME.print ("New day -> ");
          fileBME.print (clockData.rtcDay);
          fileBME.print ("/");
          fileBME.print (clockData.rtcMonth);
          fileBME.print ("/");
          fileBME.println (clockData.rtcYear);
          fileBME.close();
          
        // BME Humidity
        fileBME = SD.open ("/BME/Humidity.txt", FILE_WRITE);
        if (fileBME) 
          fileBME.print ("New day -> ");
          fileBME.print (clockData.rtcDay);
          fileBME.print ("/");
          fileBME.print (clockData.rtcMonth);
          fileBME.print ("/");
          fileBME.println (clockData.rtcYear);          
          fileBME.close();
          
        // BME Pressure
        fileBME = SD.open ("/BME/Pressure.txt", FILE_WRITE);
        if (fileBME)
          fileBME.print ("New day -> ");
          fileBME.print (clockData.rtcDay);
          fileBME.print ("/");
          fileBME.print (clockData.rtcMonth);
          fileBME.print ("/");
          fileBME.println (clockData.rtcYear);          
          fileBME.close();
     }
  }
}
