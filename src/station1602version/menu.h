static float bmePressureFlag = 0;
String daysOfTheWeek[8] = { "Sunday ", "Monday ", "Tuesday ", "Wednesday ", "Thursday ", "Friday ", "Saturday " };

// BME
struct bmeInfo {
  float temp;
  float humid;
  float pres;
};

// RTC
struct rtcData {
  int  rtcYear;
  byte rtcMonth;
  byte rtcDay;
  byte rtcHour;
  byte rtcMinute;
  byte rtcSecond;
  String rtcWeekDay;
};

void mainMenu(const rtcData &data, const bmeInfo &weather)
{
  Serial.println("Main menu start");
  // Date
  lcd.setCursor(0, 0);
  lcd.print(data.rtcWeekDay);
  lcd.setCursor(11, 0);
  lcd.print(data.rtcDay);
  lcd.print(".");
  lcd.print(data.rtcMonth);
  // Temeperature
  lcd.setCursor(9, 1);
  lcd.print(weather.temp);
  lcd.print("*C");
  // Time
  lcd.setCursor(0, 1);
  lcd.print(data.rtcHour);
  lcd.print(":");
  lcd.print(data.rtcMinute);
}

void bmeMenu(const bmeInfo &weather, float &bmePressureFlag)
{
  Serial.println("MBE menu start");
  Serial.print ("Temperature: ");
  Serial.println (weather.temp);
  Serial.print ("Humidity: ");
  Serial.println (weather.humid);
  Serial.print ("Pressure: ");
  Serial.println (weather.pres);
  
  // Temperature
  lcd.home();
  lcd.print(weather.temp);
  lcd.print("*C ");
  // Humidity
  lcd.print(weather.humid);
  lcd.print("%");
  // Pressure
  if (millis() - millTmr_3 > 1000 * 3600) {
    millTmr_3 = millis();
    lcd.setCursor(0, 1);
    if ((bmePressureFlag+1) > weather.pres) {
      lcd.print("+");
    } else if ((bmePressureFlag-1) < weather.pres) {
      lcd.print("-");
    } else if ((bmePressureFlag) == weather.pres) {
      lcd.print(" ");
    }
    bmePressureFlag = weather.pres;
  }
  
  lcd.setCursor(1, 1);
  lcd.print(weather.pres);
  lcd.print("mmHg");
}

void rtcMenu(const rtcData &data)
{
  Serial.println("RTC menu start");
  // Week day
  lcd.home();
  lcd.print(data.rtcWeekDay);
  // Data
  lcd.setCursor(11, 0);
  lcd.print(data.rtcDay);
  lcd.print(".");
  lcd.print(data.rtcMonth);
  lcd.setCursor(0, 1);
  lcd.print(data.rtcYear);
  //Time
  lcd.setCursor(8, 1);
  lcd.print(data.rtcHour);
  lcd.print(":");
  lcd.print(data.rtcMinute);
  lcd.print(":");
  lcd.print(data.rtcSecond);
  
  Serial.print (data.rtcWeekDay);
  Serial.print (" ");
  Serial.print (data.rtcYear);
  Serial.print ("/");
  Serial.print (data.rtcMonth);
  Serial.print ("/");
  Serial.print (data.rtcDay);
  Serial.print (" ");
  Serial.print (data.rtcHour); 
  Serial.print (":");
  Serial.print (data.rtcMinute);
  Serial.print (":");
  Serial.println (data.rtcSecond);
}
