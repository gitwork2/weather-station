#include <SPI.h>
#include <Wire.h>
#include <GyverBME280.h>
#include <RTClib.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <SD.h>

GyverBME280 bme;
RTC_DS3231  rtc;
File fileBME; // SD file for BME
File fileRTC; // SD file for RTC

const byte SCREEN_WIDTH = 128; // OLED display width, in pixels
const byte SCREEN_HEIGHT = 32; // OLED display height, in pixels
Adafruit_SSD1306 oled(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);

uint32_t timerMill;
const byte buttPin = D0;
static byte menuNum = 0;
String daysOfTheWeek[8] = { "Sunday ", "Monday ", "Tuesday ", "Wednesday ", "Thursday ", "Friday ", "Saturday " };
#include "menu.h"
 
void setup() {
  Serial.begin(9600);
  pinMode(buttPin, INPUT);

  // BME
  bme.begin();

  // RTC
  #ifndef ESP8266
    while (!Serial); // wait for serial port to connect. Needed for native USB
  #endif
  if (!rtc.begin()) Serial.println("Couldn't find RTC");
  if (rtc.lostPower()) {
    Serial.println("RTC lost power, let's set the time!");
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  }
  
  // OLED 1306
  if(!oled.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { // If start display
    Serial.println("SSD1306 unreachable");
  }
  oled.clearDisplay(); // Clear buffer of display
  oled.display(); // Show buffer of display

  // SD card
  const int chipSelect = 10;
  if (!SD.begin(chipSelect)) {
    Serial.println("initialization failed. Things to check:");
    Serial.println("1. is a card inserted?");
    Serial.println("2. is your wiring correct?");
    Serial.println("3. did you change the chipSelect pin to match your shield or module?");
    Serial.println("Note: press reset button on the board and reopen this Serial Monitor after fixing your issue!");
  }
}

void loop() {
/* =============================== MENU =============================== */
  DateTime now = rtc.now();

  bmeInfo weather {
    bme.readTemperature(),
    bme.readHumidity(),
    round(pressureToMmHg(bme.readPressure()) * 10) / 10
  };
    
  rtcData clockData {
    now.year(),
    now.month(),
    now.day(),
    now.hour(),
    now.minute(),
    now.second(),
    daysOfTheWeek[now.dayOfTheWeek()]
  };
  rtcMenu(clockData);
  bmeMenu(weather);

  /* Get switch press 
  Serial.println(digitalRead(buttPin));
  if (millis() - timerMill > 50) {
    timerMill = millis();
    if (digitalRead(buttPin)) { 
      ++menuNum;
    }
    if (menuNum > 2) menuNum = 0;    
  }
  
  
  oled.clearDisplay(); // Clear buffer
  oled.setTextColor(SSD1306_WHITE);
  switch (menuNum) {
    case 0:
      //printDataTime(clockData);
      //printInfo(weather);
      break;
    case 1:
      bmeMenu(weather);
      break;
    case 2:
      rtcMenu(clockData);
      break;
    default:
      break;
  }
  //oled.invertDisplay(true);
  oled.display(); // Show buffer
  */
  // Write data on SD card
  // Write BME
  SD.mkdir("/BME");

  // BME Temperature
  fileBME = SD.open ("/BME/Temperature.txt", FILE_WRITE);
  if (fileBME)
    fileBME.println (weather.temp);
    fileBME.close();
  // BME Humidity
  fileBME = SD.open ("/BME/Humidity.txt", FILE_WRITE);
  if (fileBME) 
    fileBME.println (weather.humid);
    fileBME.close();
  // BME Pressure
  fileBME = SD.open ("/BME/Pressure.txt", FILE_WRITE);
  if (fileBME)
    fileBME.println (weather.pres);
    fileBME.close();
  // RTC Data
  fileRTC = SD.open ("/RTC/Data.txt", FILE_WRITE);
    if (fileRTC)
      fileRTC.print (clockData.rtcYear);
      fileRTC.print ("/");
      fileRTC.print (clockData.rtcMonth);
      fileRTC.print ("/");
      fileRTC.println (clockData.rtcDay);
      fileRTC.close();
  // RTC Time
  fileRTC = SD.open ("/RTC/Time.txt", FILE_WRITE);
    if (fileRTC)
      fileRTC.print (clockData.rtcHour);
      fileRTC.print (":");
      fileRTC.print (clockData.rtcMinute);
      fileRTC.print (":");
      fileRTC.println (clockData.rtcSecond);
      fileRTC.close();
      
  delay(1000);
}
