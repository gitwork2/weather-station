#include <GyverBME280.h>
#include <RTClib.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

GyverBME280 bme;
RTC_DS3231  rtc;
String daysOfTheWeek[8] = {"Sunday ", "Monday ", "Tuesday ", "Wednesday ", "Thursday ", "Friday ", "Saturday "};

const byte SCREEN_WIDTH = 128; // OLED display width, in pixels
const byte SCREEN_HEIGHT = 32; // OLED display height, in pixels
Adafruit_SSD1306 oled(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);

void setup() {
  Serial.begin(9600);
  // BME
  bme.begin();
  
  // RTC
  #ifndef ESP8266
    while (!Serial); // wait for serial port to connect. Needed for native USB
  #endif

  if (! rtc.begin()) {
    Serial.println("Couldn't find RTC");
    Serial.flush();
  }

  if (rtc.lostPower()) {
    Serial.println("RTC lost power, let's set the time!");
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  }
  
  // OLED 1306
  if(!oled.begin(SSD1306_SWITCHCAPVCC, 0x3C)) {
    Serial.println("SSD1306 unreachable");
  }
  oled.clearDisplay();
  oled.display();
}

void loop() {
  // BME
  struct bmeInfo {
    float temp = bme.readTemperature();
    float humid = bme.readHumidity();
    float pres = round(pressureToMmHg(bme.readPressure()) * 10) / 10;
  }
  // RTC
  DateTime now = rtc.now();

  struct rtcData {
    byte rtcYear   = now.year();
    byte rtcMonth  = now.month();
    byte rtcDay    = now.day();
    byte rtcHour   = now.hour();
    byte rtcMinute = now.minute();
    byte rtcSecond = now.second();
    String rtcWeekDay = daysOfTheWeek[now.dayOfTheWeek()];
  }
  
/* =============================== MENU =============================== */
  oled.clearDisplay();
  oled.setTextColor(SSD1306_WHITE);
  //oled.invertDisplay(true);
  printDataTime(rtcMonth, rtcDay, rtcHour, rtcMinute, rtcWeekDay);
  printInfo(temp, humid, pres);

  
  oled.display();
  delay(10);
}

void printDataTime(byte realMonth, byte realDay, byte realHour, byte realMinute, String realWeekDay)
{
  // Time
  oled.setTextSize(2);
  oled.setCursor(0, 0);
  
  if (realHour < 10) oled.print("0");
  oled.print(realHour);
  oled.print(":");
  if (realMinute < 10) oled.print("0");
  oled.println(realMinute);

  // Date
  oled.setTextSize(2);
  oled.setCursor(68, 0);
  oled.println(realDay);
  for (int c = 0; c < 27; ++c) oled.drawPixel(65+c, 15, SSD1306_WHITE);
  oled.setCursor(68, 17);
  if (realMonth < 10) oled.setCursor(73, 17);
  oled.print(realMonth);

  oled.setTextSize(1);
  oled.setCursor(0, 20);
  oled.print(realWeekDay);
}

void printInfo(float temp, float humd, float pres)
{
  // Info
  oled.setTextColor(SSD1306_WHITE);
  oled.setTextSize(1);
  oled.setCursor(98, 0);
  oled.println(temp);

  oled.setCursor(98, 12);
  oled.println(humd);
  
  oled.setCursor(98, 24);
  oled.println(pres);
  delay(100);
}
