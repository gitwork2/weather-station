// BME
struct bmeInfo {
  float temp;
  float humid;
  float pres;
};

// RTC
struct rtcData {
  int  rtcYear;
  byte rtcMonth;
  byte rtcDay;
  byte rtcHour;
  byte rtcMinute;
  byte rtcSecond;
  String rtcWeekDay;
};

void printDataTime(rtcData data)
{
  // Time
  oled.setTextSize(2);
  oled.setCursor(0, 0);
  
  if (data.rtcHour < 10) oled.print("0");
  oled.print(data.rtcHour);
  oled.print(":");
  if (data.rtcMinute < 10) oled.print("0");
  oled.println(data.rtcMinute);

  // Date
  oled.setTextSize(2);
  oled.setCursor(68, 0);
  oled.println(data.rtcDay);
  for (int c = 0; c < 27; ++c) oled.drawPixel(65+c, 16, SSD1306_WHITE);
  oled.setCursor(68, 19);
  if (data.rtcMonth < 10) oled.setCursor(73, 19);
  oled.print(data.rtcMonth);

  oled.setTextSize(1);
  oled.setCursor(0, 21);
  oled.print(data.rtcWeekDay);
}

void printInfo(bmeInfo &weathInfo)
{
  // Info
  oled.setTextColor(SSD1306_WHITE);
  oled.setTextSize(1);
  oled.setCursor(98, 0);
  oled.println(weathInfo.temp);

  oled.setCursor(98, 12);
  oled.println(weathInfo.humid);
  
  oled.setCursor(98, 24);
  oled.println(weathInfo.pres);
  delay(100);
}

void bmeMenu(bmeInfo &weathInfo)
{
  oled.setTextColor(SSD1306_WHITE);
  oled.setTextSize(1);
  oled.setCursor(0, 0);
  oled.print("Temperatura: ");
  oled.print(weathInfo.temp);
  oled.print(" *C");

  oled.setCursor(0, 12);
  oled.print("Humidity:    ");
  oled.print(weathInfo.humid);
  oled.print(" %");

  oled.setCursor(0, 24);
  oled.print("Pressure:    ");
  oled.print(weathInfo.pres);
  oled.print("mm");

  delay(100);
}

void rtcMenu(rtcData &dataClock)
{
  oled.setTextColor(SSD1306_WHITE);
  oled.setTextSize(2);
  // Time
  oled.setCursor(0, 0);
  oled.print(dataClock.rtcHour);
  oled.print(":");
  oled.print(dataClock.rtcMinute);
  oled.print(":");
  oled.println(dataClock.rtcSecond);

  // Data
  oled.print(dataClock.rtcDay);
  oled.print("/");
  oled.print(dataClock.rtcMonth);
  oled.print("/");
  oled.print(dataClock.rtcYear);
}
