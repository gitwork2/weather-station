#include <GyverBME280.h>
#include <RTClib.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

GyverBME280 bme;
RTC_DS3231  rtc;
String daysOfTheWeek[8] = {"Sunday ", "Monday ", "Tuesday ", "Wednesday ", "Thursday ", "Friday ", "Saturday "};

const byte SCREEN_WIDTH = 128; // OLED display width, in pixels
const byte SCREEN_HEIGHT = 32; // OLED display height, in pixels
Adafruit_SSD1306 oled(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);
byte menuNum = 0;
uint32_t timerMill;
#include "menu.h"
 
void setup() {
  Serial.begin(9600);
  pinMode(D3, INPUT);
  // BME
  bme.begin();
  
  // RTC
  #ifndef ESP8266
    while (!Serial); // wait for serial port to connect. Needed for native USB
  #endif

  if (!rtc.begin()) Serial.println("Couldn't find RTC");
  if (rtc.lostPower()) {
    Serial.println("RTC lost power, let's set the time!");
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  }
  
  // OLED 1306
  if(!oled.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { // If start display
    Serial.println("SSD1306 unreachable");
  }
  oled.clearDisplay(); // Clear buffer of display
  oled.display(); // Show buffer of display
}

void loop() {
  DateTime now = rtc.now();

  bmeInfo weather {
    bme.readTemperature(),
    bme.readHumidity(),
    round(pressureToMmHg(bme.readPressure()) * 10) / 10
  };
    
  rtcData clockData {
    now.year(),
    now.month(),
    now.day(),
    now.hour(),
    now.minute(),
    now.second(),
    daysOfTheWeek[now.dayOfTheWeek()]
  };
  
/* =============================== MENU =============================== */
  // Get switch press 
  Serial.println(digitalRead(D3));
  if (millis() - timerMill > 150) {
    timerMill = millis();
    if (digitalRead(D3)) { 
      ++menuNum;
    }
    if (menuNum > 2) menuNum = 0;    
  }
  
  
  oled.clearDisplay(); // Clear buffer
  oled.setTextColor(SSD1306_WHITE);
  switch (menuNum) {
    case 0:
      printDataTime(clockData);
      printInfo(weather);
      break;
    case 1:
      bmeMenu(weather);
      break;
    case 2:
      rtcMenu(clockData);
      break;
    default:
      break;
  }
  //oled.invertDisplay(true);
  oled.display(); // Show buffer
}
