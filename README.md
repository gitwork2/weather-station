# Weather station

* Плата: Wemos 1D-mini 

* Данные: температура, влажность, давление, время.

* Носитель: microSD.

Маленькая настольная метеостанция с дисплеем.

## Структура файлов
	BME
	/BME/Temperature.txt
	/BME/Humidity.txt
	/BME/Pressure.txt

	RTC
	/RTC/Time.txt
	/RTC/Data.txt

## Схема
	BME
	SCL -> D1
	SDA -> D2

	OLED or LCD1602
	SCK -> D1
	SDA -> D2

	RTC
	C -> D1
	D -> D2

	BUTTON
	D0

	SD CARD
	MISO -> D6
	MOSI -> D7
	SOK  -> D5
	CS   -> D8

![Schema](https://gitlab.com/gitwork2/weather-station/-/blob/main/chema.png)

## Меню
Start menu

	Wednesday  14.10 - Day of week and date
	17:45    21.34*C - Time and temperature 


BME menu

	24.54*C 55.78%   - Temperature and humidity
	+754.54mmHg      - Pressure in mmHg


RTC menu

	Wednesday  21.9  - Day of week
	2023    17:30:45 - Date and time

## Web or Tg bot

ждите реализации, когда-нибудь
